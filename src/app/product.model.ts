export class ProductModel {
  name:string;
  qty:Number;
  rate:Number;
  basic:Number;
  tax:Number;
  taxAmount:Number;
  final:Number;
}
