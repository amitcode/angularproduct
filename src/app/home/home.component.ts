import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from '@angular/forms';
import { ProductModel } from '../product.model';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor() {}

  ProductArray: Array<ProductModel> = [];

  post:  {
    name: string;
    email: string;
    phone: number;
  }

  form: FormGroup;

  newProduct: any = {
    name: '',
    qty: 0,
    rate: 0,
    basic: 0,
    tax: 0,
    taxAmount: 0,
    final: 0,
  };

  tax = [
    { name: 'cgst 9%', value: 9 },
    { name: 'sgst 9%', value: 9 },
    { name: 'igst 9%', value: 9 },
  ];

  readOnly: boolean = true;
  totalAmount: number = 0;

  ngOnInit(): void {
    this.ProductArray.push(this.newProduct);

    this.form = new FormGroup({
      name: new FormControl(null, {validators: [Validators.required]
      }),
      email: new FormControl(null, { validators: [Validators.required] }),
      phone: new FormControl(null, { validators: [Validators.required] })
    });
  }

  setBasicAmount(e: any, i: Number) {
    var tr = e.target.parentNode.parentNode.childNodes;
    var qty = tr[2].childNodes[0].value;
    var rate = tr[3].childNodes[0].value;
    var basic = qty * rate;
    tr[4].childNodes[0].value = basic;

    for (var j = 1; j <= this.ProductArray.length; j++) {
      if (j === i) {
        this.ProductArray[j - 1].qty = qty;
        this.ProductArray[j - 1].rate = rate;
        this.ProductArray[j - 1].basic = basic;
      }
    }

    this.changeTax(this.tax, i, false);
  }

  changeTax(e, i, selectTax = true) {
    var basic: any = this.ProductArray[i - 1].basic;
    var tax = 0;
    var taxAmount = 0;

    if (selectTax) {
      e.forEach((t) => {
        tax += t.value;
      });
    } else {
      e.name ? e.name.forEach((v) => (tax += v)) : '';
    }

    taxAmount = (basic * tax) / 100;

    this.ProductArray[i - 1].taxAmount = taxAmount;
    this.ProductArray[i - 1].final = basic + taxAmount;

    this.setTotalAmount();
  }

  addRow() {
    let newProduct = { name: '', qty: 0, rate: 0,  basic: 0,  tax: 0,  taxAmount: 0, final: 0,};
    this.ProductArray.push(newProduct);
    return true;
  }

  deleteRow(index) {

    if (this.ProductArray.length == 1) {
      return false;
    } else {
      this.ProductArray.splice(index, 1);
    }
    this.setTotalAmount();
  }

  setTotalAmount() {
    this.totalAmount = 0;
    this.ProductArray.forEach((data: any) => {
      this.totalAmount += data.final;
    });
  }
}
