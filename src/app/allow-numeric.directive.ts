import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: 'input[AllowNumeric]'
})
export class AllowNumericDirective {

  constructor(private _el: ElementRef) { }

  @HostListener('keypress', ['$event']) keyEvent(event) {

    var charCode = event.which ? event.which : event.keyCode;

    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;

  }

}

